# Hosting a Discord.js bot on Glitch

How to host your Discord bot on Glitch and automatically deploy from GitLab

Preparations
1. GitLab repository containing your bot code
2. `<Optional>` Personal access token if your repository is private
    - Whenever you need the repository URL, use `https://oauth2:<your token here>@gitlab.com/<user>/<git project>` instead

Steps
1. Create a new glitch project
2. Set up remote repository
    1. Open the console by clicking "Logs" -> "Console"
    2. `$ git remote add origin <git repo URL>`
    3. `$ git fetch --all`
    4. `$ git reset --hard origin/master`

3. Create a webhook for push events on GitLab
    1. Go to your GitLab repository
    2. Settings -> Integrations
    3. URL = Glitch project (`https://<your-project>.glitch.me/`)
    4. Secret Token = `<Some secret text to know the event is from GitLab>`
    5. Trigger = Push events
    6. Uncheck SSL verification (IDK if necessary, I just did it)
4. Create a http server
    1. To keep the main index.js file clean, I moved the server related code to ./glitch hosting
    2. Look at that file for more details

5. Ping the server every 5 minutes to keep the bot awake at all times
    1. You could implement this within your http server, but for reliability, use an external tool
    2. I will use UptimeRobot for this (Up to 5 servers on free account, can configure ping interval between 5min - 30min)
    3. Ping the glitch URL

6. Test the GitLab webhook
    1. Go to your GitLab repository
    2. Settings -> Integrations
    3. Find your webhook
    4. Test -> Push event
    5. To see a list of recent events, click on "Edit"