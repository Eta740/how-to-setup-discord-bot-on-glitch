module.exports = prepareGlitch        //Cannot omit "module" in "module.exports"

const http = require( "http" )        //Cannot use https
const shellCMD = require( "child_process" )

function prepareGlitch () {
	const server = http.createServer( async ( req , res ) => {
		res.statusCode = 200        //Status code for successfully receiving a http request
		res.write( "Hello" )        //Some text to display on the live website for testing
		res.end()

		if ( req.headers["x-gitlab-token"] === process.env.gitlabHook ) {
		    //Condition checks for GitLab webhook "Secret Token"
		    
			updateGlitch()
		}
	} )

	const port = process.env.PORT
	server.listen( port , () => { console.log( `Listening to Port = ${ port }` ) } )
}

function updateGlitch () {
	console.log( "Received webhook from GitLab. Updating in 5 seconds..." )
	
	//If glitch refreshes immediately, the http response does not get sent and GitLab will re-send a request
	setTimeout( () => {
		shellCMD.execSync( "git fetch --all; git reset --hard origin/master; git prune; git gc; refresh" )
	} , 5000 )
}