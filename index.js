console.log( "App is starting" )

const glitchPlatform = require( "./glitch hosting" )        //Launch the http server
glitchPlatform()

const Discord = require( "discord.js" )
const client = new Discord.Client(
	{"disableEveryone" : true}
)

const discordToken = process.env.discordToken


client.on( "ready" , () => {
	console.log( `Bot is ready... ${ client.user.username }` )
} )



client.login( discordToken )